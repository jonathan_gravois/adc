<div style="margin:0; padding: 0;">
    <div class="fullframe topbar">
        <ul class="loginbar pull-right">
            <li>
                <img src="public/images/envelope.png">
                customerservice@adcportal.com
            </li>
            <li>
                <img src="public/images/phone.png">
                1.901.682.4064
            </li>
        </ul>
    </div>
</div>
<div class="container" style="margin-top: -50px !important;">
    <!-- Logo -->
    <a class="logo" href="/">
        <img src="public/images/adc/logo.png" alt="Logo">
    </a>
    <!-- End Logo -->

    <!-- Toggle get grouped for better mobile display -->
    <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-responsive-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="fa fa-bars"></span>
    </button>
    <!-- End Toggle -->
</div><!--/end container-->

<div class="collapse navbar-collapse mega-menu navbar-responsive-collapse">
    <div class="container">
        <ul class="nav navbar-nav">
            <li class="active">
                <a href="/">
                    Home
                </a>
            </li>
            <li>
                <a href="/about">
                    About Us
                </a>
            </li>
            <li>
                <a href="/customers">
                    Customers
                </a>
            </li>
            <li>
                <a href="/market">
                    ADC Marketplace
                </a>
            </li>
            <li>
                <a href="/contact">
                    Contact Us
                </a>
            </li>


            <!-- Login -->
            <li>&nbsp;</li>
            <li class="pull-right" style="margin: -30px 30px 0 0;">
                <a class="naked-link" href="{{ env('ECO_LINK') }}">
                    <img src="public/images/signin.png" alt="Login to AeroEco">
                </a>
            </li>
            <!-- Login -->
        </ul>
    </div><!--/end container-->
</div><!--/navbar-collapse-->
