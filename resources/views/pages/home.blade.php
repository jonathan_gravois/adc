@extends('adc')

@section('title', 'Aerospace Disassembly Consortium')

@section('content')
    <div class="fullframe">
        <div id="fader__box" style="z-index: 1;">
            <div class="fadein">
                <img class="bottom" src="public/images/sellers_plane.png" alt="For Sellers" style="width:100%; height: auto;">
                <img class="top" src="public/images/buyers_plane.png" alt="For Buyers" style="width:100%; height: auto;">
            </div>
        </div>
    </div>

    <div class="mainframe" style="position: relative; margin-top: -43px;">
        <div class="green_bar" style="z-index: 3;margin-left:90px;">
            <img src="public/images/green_button_bar.png" alt="AeroEco Portal" style="width:845px; max-width:845px; height: auto;" usemap="#map">
            <map name="map">
                <area shape="rect" coords="0,0,282,163" href="http://www.google.com" target="_blank" />
                <area shape="rect" coords="284,0,566,163" href="http://www.yahoo.com" target="_blank" />
                <area shape="rect" coords="568,0,850,163" href="http://www.bing.com" target="_blank" />
            </map>
        </div>

        <hr class="adc-sections">

        <div class="home-fingertips">
        <h2 class="adc-sections-green">
            Access at Your Fingertips
        </h2>
        <p>
            With ADC, all your aircraft data is available at your fingertips 24 X 7 X 365. Track the progress of the disassembly, track orders from within the Consortium, and approve purchase requests all in the same portal, AeroEco, before the components are issued to the open market.
        </p>
        <p>
            <img src="public/images/adc/computers.png" alt="AeroEco Portal">
            <a class="naked-link" href="{{ env('ECO_LINK') }}">
                <img src="public/images/signin.png" alt="AeroEco Log In">
            </a>
        </p>
    </div>

        <hr class="adc-sections">

        <div class="home-submit">
        <h2 class="adc-sections">
            Submit An Aircraft for Disassembly
        </h2>
        <form class="form-inline">
            <div class="form-group">
                <input type="text" id="email" name="email" class="form-control" placeholder="Enter your email address" style="width: 400px; display: none;">
                <input type="text" id="liame" name="liame" class="form-control" placeholder="Enter your email address" style="width: 400px; display: inline;">
            </div>
            <button type="submit" class="btn btn-uam">Begin</button>
        </form>
    </div>

        <hr class="adc-sections">

        <!-- Owl Clients v1 -->
        <h2 class="adc-sections">
            Members
        </h2>

        <div class="owl-clients-v1">
            <div class="item">
                <img src="public/images/clients/1.png" alt="">
            </div>
            <div class="item">
                <img src="public/images/clients/2.png" alt="">
            </div>
            <div class="item">
                <img src="public/images/clients/3.png" alt="">
            </div>
            <div class="item">
                <img src="public/images/clients/5.png" alt="">
            </div>
            <div class="item">
                <img src="public/images/clients/4.png" alt="">
            </div>
        </div>
        <!-- End Owl Clients v1 -->
</div>
@stop

@push('scripts')
<script>
    $(function() {
        $('img[usemap]').maphilight({stroke: false, fillColor: 'FFFFFF', fillOpacity: '0.1'});

        $('form').submit(function(e){
            var spam = $('#email').val();
            var email = $('#liame').val();

            if(spam.length === 0) {
                if(email.length === 0) {
                    $.post('/potentials', {
                        email: email
                    });
                } else {
                    alert('You must enter an email address to continue');
                }
                window.location = '/ecohome';
            }
            e.preventDefault();
        });
    });
</script>
@endpush