@extends('adc')

@section('title', 'Aerospace Disassembly Consortium')

@section('body_class', 'pricing__tabs')

@section('content')
    <div class="row mainframe">
        <div class="price__pane col-xs-6 col-sm-6 col-md-4 col-lg-4">
            <!-- TIER 2 -->
            <div class="panel price panel-red">
                <div class="panel-heading text-center">
                    <h3>FIRST CLASS</h3>
                </div>
                <div class="panel-body text-center">
                    <p class="lead" style="font-size:32px"><strong>$2,000 / year</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                </ul>
                <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-uam" href="{{ env('ECO_LINK') }}/auth/register/2">BUY NOW!</a>
                </div>
            </div>
            <!-- /PRICE ITEM -->
        </div>
        <div class="price__pane col-xs-6 col-sm-6 col-md-4 col-lg-4">
            <!-- TIER 3 -->
            <div class="panel price panel-red">
                <div class="panel-heading  text-center">
                    <h3>BUSINESS CLASS</h3>
                </div>
                <div class="panel-body text-center">
                    <p class="lead" style="font-size:32px"><strong>$1000 / month</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                </ul>
                <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-warning" href="{{ env('ECO_LINK') }}/auth/register/3">BUY NOW!</a>
                </div>
            </div>
            <!-- /PRICE ITEM -->
        </div>
        <div class="price__pane col-xs-6 col-sm-6 col-md-4 col-lg-4">
            <!-- TIER 4 -->
            <div class="panel price panel-red">
                <div class="panel-heading  text-center">
                    <h3>ECONOMY</h3>
                </div>
                <div class="panel-body text-center">
                    <p class="lead" style="font-size:32px"><strong>FREE!!!</strong></p>
                </div>
                <ul class="list-group list-group-flush text-center">
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                    <li class="list-group-item"><i class="icon-ok text-danger"></i> Cool Feature</li>
                </ul>
                <div class="panel-footer">
                    <a class="btn btn-lg btn-block btn-danger" href="{{ env('ECO_LINK') }}/auth/register/4">BUY NOW!</a>
                </div>
            </div>
            <!-- /PRICE ITEM -->
        </div>
    </div>

    <div class="row mainframe">
        <br>
        <div class="col-xs-12 text-center">
            <a class="naked_link" href="{{ env('ECO_LINK') }}/auth/register/5" style="font-size: 18px;">
                Not convinced? Click here for a 3 day FREE preview and you WILL be.
            </a>
            <br><br>
        </div>
    </div>
@endsection

@push('scripts')
<script>
    $(function() {
        //alert('jQuery');
    });
</script>
@endpush