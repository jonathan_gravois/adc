<!DOCTYPE html>
<html>
<head>
    <title> @yield('title') </title>
    <script src="https://use.typekit.net/eqs2vwj.js"></script>
    <script>try{Typekit.load({ async: true });}catch(e){}</script>
    {!! HTML::style('public/css/app.css') !!}
    <link type="image/x-icon" href="favicon.ico" rel="shortcut icon" />
</head>
<body class="@yield('body_class')">
<div class="wrapper">
    <div class="header">
        @include('pages.partials.header')
    </div>

    <div>
        @yield('content')
    </div>

    <div class="footer">
        @include('pages.partials.footer')
    </div>
</div>
</body>
{!! HTML::script('public/js/app.js') !!}
{!! HTML::script('public/js/jquery.maphilight.min.js') !!}
<!-- App scripts -->
@stack('scripts')
<!-- App scripts -->
</html>