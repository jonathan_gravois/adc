<?php

namespace App;

use Illuminate\Auth\Authenticatable;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Auth\Passwords\CanResetPassword;
use Illuminate\Foundation\Auth\Access\Authorizable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Contracts\Auth\Access\Authorizable as AuthorizableContract;
use Illuminate\Contracts\Auth\CanResetPassword as CanResetPasswordContract;

class User extends Model implements AuthenticatableContract,
                                    AuthorizableContract,
                                    CanResetPasswordContract
{
    use Authenticatable, Authorizable, CanResetPassword;

    protected $table = 'users';
    protected $fillable = ['firstname', 'lastname', 'email', 'password', 'is_admin', 'can_sell', 'role', 'tier', 'location'];
    protected $hidden = ['password', 'remember_token', 'updated_at'];

    /* CASTING */
    protected $casts = [
        'is_admin' => 'boolean',
        'can_buy' => 'boolean',
        'can_sell' => 'boolean',
        'role_id' => 'integer',
        'tier' => 'integer',
        'location_id' => 'integer'
    ];
    /* CASTING */

    /* RELATIONSHIPS */
    public function company()
    {
        return $this->belongsTo('App\Models\Company');
    }
    public function location()
    {
        return $this->belongsTo('App\Models\Location');
    }
    public function phones()
    {
        return $this->hasMany('App\Models\Phone');
    }
    public function role()
    {
        return $this->hasOne('App\Models\Role', 'id', 'role_id');
    }
    /* RELATIONSHIPS */

    /* METHODS */
    /* METHODS */
}
