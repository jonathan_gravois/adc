<?php

namespace App\Http\Controllers;

use App\Models\Ata;
use App\Http\Controllers\Controller;
use App\Http\Requests;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Maatwebsite\Excel\Facades\Excel;
use TCPDF;

class AtasController extends ApiController
{
    protected $records;

    public function __construct(Ata $records)
    {
        $this->records = $records;
    }

    public function index()
    {
        // show all
        $records = Ata::orderBy('chapter')->orderBy('section')->get();
        return $records;
    }

    public function chapters()
    {
        // show all
        $records = Ata::where('section', 'LIKE', '00')->orderBy('chapter')->get();
        return $records;
    }

    public function destroy($id)
    {
        // delete single
        $record = $this->records->findOrFail($id);
        $record->delete();
        return $this->respondOK('Ata was deleted');
    }

    public function show($id)
    {
        //show single
        $record = $this->records->findOrFail($id);
        return $record;
    }

    public function store()
    {
        // insert new
        $record = Ata::create(Input::all());
        return $this->respond($record->id);
    }

    public function update($id)
    {
        // save updated
        $record = $this->records->find($id);

        if(! $record){
            Ata::create(Input::all());
            return $this->respond($record);
        }

        $record->fill(Input::all())->save();
        return $this->respond($record);
    }
}
