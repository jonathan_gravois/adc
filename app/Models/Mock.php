<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Mock extends Model
{
    /*public $timestamps = false;*/
    protected $table = 'mocks';
    protected $fillable = ['model', 'engine_count', 'manufacturer'];
    /*protected $hidden = ['created_at', 'updated_at'];*/

    /* CASTING */
    protected $casts = [
        'engine_count' => 'integer'
    ];
    /* CASTING */

    /* RELATIONSHIPS */
    /* RELATIONSHIPS */

    /* METHODS */
    /* METHODS */
}